CC = gcc
MPICC = mpicc
LIBS = -lm
UNAME_S = $(shell uname -s)

ifneq (, $(findstring SunOS, $(UNAME_S)))
	LIBS += -lnsl -lsocket -lresolv
endif
EXETARGETSTEMS=dtlz2_serial dtlz2_advanced frontend

ifeq (Darwin, $(UNAME_S))
	CFLAGS = -O3
	SONAME=dylib
	EXETARGETS=$(EXETARGETSTEMS)
	DTLZ2_MS=dtlz2_ms
	LDFLAGS=
else ifeq (Linux, $(UNAME_S))
	CFLAGS = -O3 -fPIC
	SONAME=so
	EXETARGETS=$(EXETARGETSTEMS)
	DTLZ2_MS=dtlz2_ms
	LDFLAGS = -Wl,-R,\.
else
	CFLAGS = -O3
	SONAME=dll
	EXETARGETS=$(addsuffix .exe,$(EXETARGETSTEMS))
	DTLZ2_MS=dtlz2_ms.exe
	LDFLAGS = -Wl,-R,\.
endif

all: $(EXETARGETS)

shared: libborg.$(SONAME)

%: %.c borg.c mt19937ar.c
	$(CC) $(CFLAGS) $^ $(LIBS) -o $@

libborg.$(SONAME): borg.c mt19937ar.c
	$(CC) -shared $(CFLAGS) $^ $(LIBS) -o $@

$(DTLZ2_MS): dtlz2_ms.c borgms.c mt19937ar.c
	$(MPICC) $(CFLAGS) $^ $(LIBS) -o $@

clean:
	-rm $(EXETARGETS) $(DTLZ2_MS) libborg.$(SONAME)

.PHONY: all clean
